"""
File: bigin_file_generator.py

This routine generates a big.in file that can be used as an input for the Mercury6 simulations.
The orbital elements are in cartesian coordinates. 

Author: Sarah Joiret

Version   Date            Editor          Comment
-------   ----------   --------------   ----------------------------------------------------------
0.1       29 Nov 2021   Sarah Joiret     First tests.
0.2       12 Apr 2022   Sarah Joiret     Added the outer planetesimal disk and an automatic way to 
                                         generate the 10 different folders for each specific 
                                         configuration

"""

import numpy as np
import os
from astropy.constants import G, M_earth, M_sun, au 
from math import pi
import shutil

# =================================================================================================
# In this section a few auxiliary functions are defined which are needed for the routine
def mass_generator(nb_Earth, nb_pp, nb_pl):
    """
    Generates the mass of the terrestrial protoplanets (embryos) and planetesimals

    Parameters
    ----------
    nb_Earth : float
        Number of Earth masses for the inner planetesimal disk
    nb_pp : integer
        Number of terrestrial protoplanets
    nb_pl : integer
        Number of planetesimals

    Returns
    -------
    M_pp: float
        Mass of a terrestrial protoplanets
    M_pl: float
        Mass of a planetesimal

    """
    M_earth_SU = M_earth.value / M_sun.value  # Earth's mass in solar units
    M_tot = nb_Earth * M_earth_SU  # Total mass of embryos + planetesimals in solar units
    
    M_pp_tot = 0.9 * M_tot # Total mass of embryos
    M_pp = M_pp_tot / nb_pp  # Mass of each planetary embryo
    
    M_pl_tot = 0.1 * M_tot # Total mass of planetesimals
    M_pl = M_pl_tot / nb_pl  # Mass of each planetesimal
    
    return M_pp, M_pl

def massc_generator(nbc_Earth, nb_co):
    """
    Generates the mass of the terrestrial protoplanets (embryos) and planetesimals

    Parameters
    ----------
    nbc_Earth : float
        Number of Earth masses for the outer planetesimal disk
    nb_co : integer
        Number of outer planetesimals

    Returns
    -------
    M_co: float
        Mass of outer planetesimal

    """
    M_earth_SU = M_earth.value / M_sun.value  # Earth's mass in solar units
    M_tot = nb_Earth * M_earth_SU  # Total mass outer planetesimals in solar units

    M_co = M_tot / nb_co  # Mass of each outer planetesimal
    
    return M_co

def a_annulus(r_in, r_out, n):
    """
    Generates n random semi-major axes (uniformly distributed) within an annulus of 
    inner radius r_in and outer radius r_out
    Case: "Annulus" (Grand Tack, Broz et al., Low-mass asteroid belt)

    Parameters
    ----------
    r_in : float
        Inner radius of the annulus of terrestrial protoplanets / planetesimals
    r_out : float
        Outer radius of the annulus of terrestrial protoplanets / planetesimals
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    a: float
        semi-major axes of the terrestrial protoplanets

    """
    a = np.random.uniform(r_in, r_out, n)
    return a

def e_i_generator(n):
    """
    Generates n random eccentricities and inclinations (following a Raileigh distribution)
    cf. what has been done in Nesvorny et al. (2020), and also Minton & Levison (2014)

    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    e, i: float
        eccentricty and inclination of the terrestrial protoplanets

    """
    e = np.random.rayleigh(0.005, n) # The value of 0.005 is taken from Nesvorny et al. (2020)
    i = np.random.rayleigh(0.0025, n) # The value of 0.0025 is taken from Nesvorny et al. (2020)
    return e, i

def g_n_generator(n):
    """
    Generates n random arguments of pericentre and longitudes of ascending node 
    uniformly distributed between 0 and 360 degrees (0 is included and 360 is excluded)
    
    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    g, n: float
        argument of pericentre (in degrees) and longitude of ascending node (in degrees)

    """
    g = np.random.uniform(0, 360, n)
    n = np.random.uniform(0, 360, n)
    return g, n

def M_generator(n):
    """
    Generates n random mean anomalies uniformly distributed between 0 and 360 degrees 
    (0 is included and 360 is excluded)
    
    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    M: float
         mean anomaly (in degrees)

    """
    M = np.random.uniform(0, 360, n)
    return M

def kep_2_cart(a, e, i, g, n, M):
    """
    Converts Keplerian coordinates to cartesian coordinates

    Parameters
    ----------
    a : float
        semi-major axis (in AU)
    e : float
        eccentricty
    i : float
        inclination (degrees)
    g : float
        argument of pericentre (degrees)
    n : float
        longitude of ascending node (degrees)
    M : float
        mean anomaly (degrees) ≃ true anomaly for circular orbits

    Returns
    -------
    x, y, z, vx, vy, vz: float
        cartesian coordinates of the protoplanets / planetesimals 

    """
    # Convert degrees into radians
    i = np.deg2rad(i)
    g = np.deg2rad(g)
    n = np.deg2rad(n)
    M = np.deg2rad(M)
    
    # Compute the radius r in AU
    r= a * (1 - e**2) / (1 + (e * np.cos(M))) 
    
    # Compute the specific angular momentum h
    mu_ms = G.value * M_sun.value # Standard gravitational parameter = GM of the central body
                                  # with G the Newtonian constant of gravitation (in m^3s^-2) 
    mu = mu_ms * (86400**2) / ((au.value)**3)    # There are 86400 s in a Julian day                         
    h = np.sqrt(mu * a *(1 - e**2)) 
    
    # Compute the position components x, y, z (in AU)
    x = r * (np.cos(n) * np.cos(g + M) - np.sin(n) * np.sin(g + M) * np.cos(i))
    y = r * (np.sin(n) * np.cos(g + M) + np.cos(n) * np.sin(g + M) * np.cos(i))
    z = r * np.sin(i) * np.sin(g + M) 
    
    # Compute the velocity components vx, vy, vz (in AU/day)
    vx = (x * h * e * np.sin(M) / (r * a * (1 - e**2))) - (h * (np.cos(n) * np.sin(g + M) + np.sin(n) * np.cos(g + M) * np.cos(i)) / r)
    vy = (y * h * e * np.sin(M) / (r * a * (1 - e**2))) - (h * (np.sin(n) * np.sin(g + M) - np.cos(n) * np.cos(g + M) * np.cos(i)) / r)
    vz = (z * h * e * np.sin(M) / (r * a * (1 - e**2))) + (h * np.sin(i) * np.cos(g + M) / r) 
    return x, y, z, vx, vy, vz
    
def xy_generator(r_in, r_out, n):
    """
    Generates n random x and y coordinates (uniformly distributed) within an annulus of 
    inner radius r_in and outer radius r_out
    => might not be useful if we generate keplerian coordinates and then convert it into 
    cartesian coordinates

    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals
    r_in : float
        Inner radius of the annulus of terrestrial protoplanets / planetesimals
    r_out : TYPE
        Outer radius of the annulus of terrestrial protoplanets / planetesimals

    Returns
    -------
    x, y : float
        x and y coordinates of the terrestrial planetesimals

    """
    angle = np.random.uniform(0., 2 * pi, n) # In radians
    r_squared = np.random.uniform(r_in ** 2, r_out ** 2, n)  # r_in^2 ≤ r^2 ≤ r_out^2  and r^2 = x^2 + y^2 
    r = np.sqrt(r_squared)
    x = r * np.cos(angle)
    y = r * np.sin(angle)
    return x, y

def z_generator(mu, sigma, n):
    """
    Generates n random z coordinates (normally distributed)
    => might not be useful if we generate keplerian coordinates and then convert it into 
    cartesian coordinates

    Parameters
    ----------
    mu: float
        Mean of the normal distribution
    sigma: float
        Standart deviation of the normal distribution
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    z : float
        z coordinates of the terrestrial protoplanets / planetesimals 

    """
    z = np.random.normal(mu, sigma, n)
    return z

def vel_generator(mu, sigma, n):
    """
    Generates n random vx, vy and vz coordinates (normally distributed)
    => might not be useful if we generate keplerian coordinates and then convert it into 
    cartesian coordinates

    Parameters
    ----------
    mu: float
        Mean of the normal distribution
    sigma: float
        Standart deviation of the normal distribution
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    vx, vy, vz : float
        vx, vy, vz coordinates of the terrestrial protoplanets / planetesimals 

    """
    vx = np.random.normal(mu, sigma, n)
    vy = np.random.normal(mu, sigma, n)
    vz = np.random.normal(mu, sigma, n)
    return vx, vy, vz


# ------------------------------------------------------------------------------------------------
# Define default input values
# ------------------------------------------------------------------------------------------------

# Configuration of the inner solar system
PROTOPLANETS = True
PLANETESIMALS = True 
BIG = True  # True if case big, false if case small
if BIG == True:
    SMALL = False
else:
    SMALL = True

# Configuration of the giant planets (choose the big.in file of one of Matt Clement's successful simulation for the giant planets)
GIANT_actual = False
GIANT_38 = False
GIANT_143 = False

# Configuration of the comets
COMETS = False

# Input parameters for the terrestrial protoplanets and planetesimals; and the comets (can be changed)
if BIG:
    nb_pp = 20   # Number of protoplanets = 20 in the "big case"
if SMALL:
    nb_pp = 40   # Number of protoplanets = 40 in the "small case" 

nb_pl = 500   # Number of inner planetesimals
nb_co = 10000   # Number of outer planetesimals (comets)

nb_Earth = 2.5   # Number of Earth masses for the terrestrial protoplanets (embryos) and the planetesimals
r_in = 0.7   # In AU
r_out = 1.2   # In AU

nbc_Earth = 25    # Number of Earth masses for the outer planetesimals (comets)
rc_in = 21   # In AU (for the outer disk)
rc_out = 30   # In AU (for the outer disk)

# Input values for the terrestrial protoplanets and planetesimals (should not be changed)
r_pp = '1.00000E+00' # Max distance from a protoplanet (in Hill raddii) that constitutes a close encounter
d_pp = '3.00000E+00' # Density of the protoplanets in g/cm^3  
d_pl = '2.00000e+00' # Density of the inner planetesimals in g/cm^3 
d_co = '0.50000e+00' # Density of the outer planetesimals (comets) in g/cm^3 
M_pp = str(mass_generator(nb_Earth, nb_pp, nb_pl)[0])  # Mass of each protoplanet in Solar Masses
M_pl = str(mass_generator(nb_Earth, nb_pp, nb_pl)[1])  # Mass of each planetesimal in Solar Masses (not useful for the big.in file)
M_co = str(massc_generator(nbc_Earth, nb_co)) # Mass of each outer planetesimal (comet) in Solar Masses
S_pp = '0.000000000000000E+00'



# =================================================================================================
# In this section 10 folders are generated in a specific directory
for nb in range(1, 11):
    def_dir = "/Users/sarahjoiret/Desktop/sims/mercury/case2/small/nojov/"
    os.mkdir(def_dir + str(nb))
    default_out_dir = def_dir + str(nb)
    
    # ------------------------------------------------------------------------------------------------
    # Copy all the input files necessary for a mercury run in each 10 folders
    # element.in, close.in, files.in, message.in, param.in, swift.inc, mercury.inc, element6.for, 
    # close6.for, mercury6_2.for and mercury6.job are in the input_files folder
    # ------------------------------------------------------------------------------------------------
    source_dir = "/Users/sarahjoiret/Desktop/sims/mercury/input_files/"
    
    # Fetch all files
    for filename in os.listdir(source_dir):
        source = source_dir + filename
        destination = default_out_dir + "/" + filename
        
        # Copy only files 
        if os.path.isfile(source):
            shutil.copy(source, destination)
    
    # =================================================================================================
    # In this section the big.in files are generated
    # ------------------------------------------------------------------------------------------------
    # Create the big.in file in a specific directory, open it and write in it 
    # ------------------------------------------------------------------------------------------------
    default_file_name = "big.in"
    file_path = os.path.join(default_out_dir, default_file_name)
    file_big = open(file_path, "w+")
    
    file_big.write(")O+_05 Big-body initial data  (WARNING: Do not delete this line!!) \
                   \n) Lines beginning with `)' are ignored. \
                   \n)---------------------------------------------------------------------\
                   \n  style (Cartesian, Asteroidal, Cometary) = Cartesian \
                   \n  epoch (in days) =     0000.0000 \
                   \n)---------------------------------------------------------------------\n")
        
    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the giant planets 
    # For GIANT_actual: taken from the Horizon jpl website for the cartesian coordinates (https://ssd.jpl.nasa.gov/horizons/app.html#/) on 2022-Apr-12 00:00:00.0000
    # For GIANT_38 and GIANT_143: using the big.in file used in one of Matt Clement's successful simulation: /just_gps/38 or /just_gps/143
    # r indicates the max distance from the body (in Hill raddii) that constitutes a close encounter (default is r=1)
    # d indicates the density of the body in g/cm^3 (default is d=1)
    # m indicates the body's mass in Solar masses (if a value is not specified, the mass is assumed to be 0)
    # ------------------------------------------------------------------------------------------------
    if GIANT_actual:
        giant_actual_dict = {}
        
        giant_actual_dict['name'] = np.array(['JUPITER', 'SATURN', 'URANUS', 'NEPTUNE'])
        giant_actual_dict['r'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['m'] = np.array(['0.954324237111000E-03', '0.285716656000000E-03', '0.436500000000000E-04', '0.514900000000000E-04'])
        giant_actual_dict['x'] = np.array(['4.856667788838316E+00', '7.310566318127806E+00', '1.411169967197528E+01', '2.966424563924522E+01'])
        giant_actual_dict['y'] = np.array(['-1.025488061152929E+00', '-6.658412608528993E+00', '1.375076475280445E+01', '-3.771098136105441E+00'])
        giant_actual_dict['z'] = np.array(['-1.044013501452873E-01', '-1.752918068843707E-01', '-1.317490569422967E-01', '-6.059837711964712E-01'])
        giant_actual_dict['vx'] = np.array(['1.469214244356701E-03', '3.443626331268295E-03', '-2.773830926096617E-03', '3.754365324365258E-04'])
        giant_actual_dict['vy'] = np.array(['7.736375041500906E-03', '4.113765988889820E-03', '2.633722272323927E-03', '3.133090911420504E-03'])
        giant_actual_dict['vz'] = np.array(['-6.500748338995526E-05', '-2.088257913410473E-04', '4.573510209027578E-05', '-7.307704001256925E-05'])
        giant_actual_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the big.in file
        for i in range(len(giant_actual_dict['name'])):
            file_big.write(giant_actual_dict['name'][i] + ' r=' + giant_actual_dict['r'][i] + ' d=' + giant_actual_dict['d'][i] + ' m= ' + giant_actual_dict['m'][i] + '\n' \
                           + giant_actual_dict['x'][i] + ' ' + giant_actual_dict['y'][i] + ' ' + giant_actual_dict['z'][i] + '\n' \
                               + giant_actual_dict['vx'][i] + ' ' + giant_actual_dict['vy'][i] + ' ' + giant_actual_dict['vz'][i] + '\n'\
                                   + giant_actual_dict['Sx'][i] + ' ' + giant_actual_dict['Sy'][i] + ' ' + giant_actual_dict['Sz'][i] + '\n') 
    
    if GIANT_38:
        giant_38_dict = {}   
         
        giant_38_dict['name'] = np.array(['JUPITER', 'SATURN', 'ICE1', 'ICE2', 'ICE3'])
        giant_38_dict['r'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_38_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_38_dict['m'] = np.array(['0.954324237111000E-03', '0.285716656000000E-03', '0.480480000000000E-04', '0.480480000000000E-04', '0.480480000000000E-04'])
        giant_38_dict['x'] = np.array(['-2.429996984653656E+00', '4.192870362769924E+00', '6.979917018524623E+00', '2.388500283260020E+00', '-1.880470093752197E+01'])
        giant_38_dict['y'] = np.array(['5.079305272871790E+00', '6.108759377422657E+00', '7.284090934005891E+00', '1.472998248685347E+01', '5.675850687044249E+00'])
        giant_38_dict['z'] = np.array(['-1.039580634462422E-02', '4.081530226173089E-02', '-2.283290142035875E-02', '3.354898062378808E-02', '1.440636629894898E-02'])
        giant_38_dict['vx'] = np.array(['-6.520221349039834E-03', '-5.309805835150783E-03', '-3.913585709787513E-03', '-4.404537016468682E-03', '-1.132396654573663E-03'])
        giant_38_dict['vy'] = np.array(['-3.130955690045265E-03', '3.531010329994419E-03', '3.676860702029279E-03', '7.240862456201549E-04', '-3.725889910586572E-03'])
        giant_38_dict['vz'] = np.array(['3.902651051299332E-05', '-3.466109798314068E-05', '8.284835522025843E-05', '2.586796226413434E-05', '3.048245438228368E-05'])
        giant_38_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_38_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_38_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])

        # Write the cartesian coordinates of the giant planets in the big.in file
        for i in range(len(giant_38_dict['name'])):
            file_big.write(giant_38_dict['name'][i] + ' r=' + giant_38_dict['r'][i] + ' d=' + giant_38_dict['d'][i] + ' m= ' + giant_38_dict['m'][i] + '\n' \
                           + giant_38_dict['x'][i] + ' ' + giant_38_dict['y'][i] + ' ' + giant_38_dict['z'][i] + '\n' \
                               + giant_38_dict['vx'][i] + ' ' + giant_38_dict['vy'][i] + ' ' + giant_38_dict['vz'][i] + '\n'\
                                   + giant_38_dict['Sx'][i] + ' ' + giant_38_dict['Sy'][i] + ' ' + giant_38_dict['Sz'][i] + '\n') 
        
    if GIANT_143:
        giant_143_dict = {}
        
        giant_143_dict['name'] = np.array(['JUPITER', 'SATURN', 'ICE1', 'ICE2', 'ICE3', 'ICE4'])
        giant_143_dict['r'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_143_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_143_dict['m'] = np.array(['0.954324237111000E-03', '0.285716656000000E-03', '0.480480000000000E-04', '0.480480000000000E-04', '0.480480000000000E-04', '0.480480000000000E-04'])
        giant_143_dict['x'] = np.array(['3.095850614036230E+00', '3.537254192848035E+00', '-5.929637101654907E+00', '8.415734275299615E+00', '1.043048252911792E+01', '-1.740128260125512E+01'])
        giant_143_dict['y'] = np.array(['-4.197569114358428E+00', '-8.215670364542870E+00', '-8.442260349049546E+00', '-9.851457565623100E+00', '-1.222863747097563E+01', '1.197201804635063E+01'])
        giant_143_dict['z'] = np.array(['1.282071428217160E-02', '-3.094879856924387E-02', '-2.657166108460747E-01', '1.291591953805336E-01', '1.349958196615994E-02', '-4.919656560307008E-02'])
        giant_143_dict['vx'] = np.array(['6.387556143196142E-03', '5.127775085715521E-03', '4.413098964364965E-03', '3.694582703664088E-03', '3.279215324398084E-03', '-2.114316952283473E-03'])
        giant_143_dict['vy'] = np.array(['4.464651668898608E-03', '2.606959430313674E-03', '-3.255347502745588E-03 ', '3.104100980066021E-03', '2.751728676174589E-03', '-3.058890324262810E-03'])
        giant_143_dict['vz'] = np.array(['-1.318942873847476E-05', '-8.196479578723253E-06', '-3.356351873300561E-07', '6.475498522226353E-06', '-3.346051792376071E-06', '1.677697727432109E-05'])
        giant_143_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_143_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_143_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the big.in file
        for i in range(len(giant_143_dict['name'])):
            file_big.write(giant_143_dict['name'][i] + ' r=' + giant_143_dict['r'][i] + ' d=' + giant_143_dict['d'][i] + ' m= ' + giant_143_dict['m'][i] + '\n' \
                           + giant_143_dict['x'][i] + ' ' + giant_143_dict['y'][i] + ' ' + giant_143_dict['z'][i] + '\n' \
                               + giant_143_dict['vx'][i] + ' ' + giant_143_dict['vy'][i] + ' ' + giant_143_dict['vz'][i] + '\n'\
                                   + giant_143_dict['Sx'][i] + ' ' + giant_143_dict['Sy'][i] + ' ' + giant_143_dict['Sz'][i] + '\n') 
            
    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the protoplanets
    # ------------------------------------------------------------------------------------------------ 
    if PROTOPLANETS:
        pp_dict = {}

        # Generate the names of the protoplanets
        list_of_names = []
        name = "E{number}"  # E stands for terrestrial planet embryos
        for i in range(nb_pp):
            list_of_names.append(name.format(number = i))    

        # Generate the lists of r, d, m, a, e, i, g (argument of pericenter), n (longitude of ascending node), M (mean anomaly), S (spin)
        list_of_r = [r_pp for i in range(nb_pp)]
        list_of_d = [d_pp for i in range(nb_pp)]
        list_of_m = [M_pp for i in range(nb_pp)]
        list_of_a = np.array(a_annulus(r_in, r_out, nb_pp))
        list_of_e = np.array(e_i_generator(nb_pp)[0])
        list_of_i = np.array(e_i_generator(nb_pp)[1])
        list_of_g = np.array(g_n_generator(nb_pp)[0])
        list_of_n = np.array(g_n_generator(nb_pp)[1])
        list_of_M = np.array(M_generator(nb_pp))
        list_of_S = [S_pp for i in range(nb_pp)]

        # Iitialize the lists for converting keplerian coordinates to cartesian coordinates
        list_of_x = np.ones(nb_pp)
        list_of_y = np.ones(nb_pp)
        list_of_z = np.ones(nb_pp)
        list_of_vx = np.ones(nb_pp)
        list_of_vy = np.ones(nb_pp)
        list_of_vz = np.ones(nb_pp)

        # Convert keplerian coordinates to cartesian coordinates
        for j in range(nb_pp):
            a = list_of_a[j]
            e = list_of_e[j]
            i = list_of_i[j]
            g = list_of_g[j]
            n = list_of_n[j]
            M = list_of_M[j]
            
            x_j = kep_2_cart(a, e, i, g, n, M)[0]
            y_j = kep_2_cart(a, e, i, g, n, M)[1]
            z_j = kep_2_cart(a, e, i, g, n, M)[2]
            vx_j = kep_2_cart(a, e, i, g, n, M)[3]
            vy_j = kep_2_cart(a, e, i, g, n, M)[4]
            vz_j = kep_2_cart(a, e, i, g, n, M)[5]
            
            list_of_x[j] = x_j
            list_of_y[j] = y_j
            list_of_z[j] = z_j
            list_of_vx[j] = vx_j
            list_of_vy[j] = vy_j
            list_of_vz[j] = vz_j
            
        # In the dictionnary, put the lists as arrays of strings
        pp_dict['name'] = np.array(list_of_names)
        pp_dict['r'] = np.array(list_of_r)
        pp_dict['d'] = np.array(list_of_d)
        pp_dict['m'] = np.array(list_of_m)
        pp_dict['x'] = np.asarray(list_of_x, dtype=str)
        pp_dict['y'] = np.asarray(list_of_y, dtype=str)
        pp_dict['z'] = np.asarray(list_of_z, dtype=str)
        pp_dict['vx'] = np.asarray(list_of_vx, dtype=str)
        pp_dict['vy'] = np.asarray(list_of_vy, dtype=str)
        pp_dict['vz'] = np.asarray(list_of_vz, dtype=str)
        pp_dict['Sx'] = np.array(list_of_S)
        pp_dict['Sy'] = np.array(list_of_S)
        pp_dict['Sz'] = np.array(list_of_S)

        # Write the cartesian coordinates of the terrestrial protoplanets in the big.in file
        for i in range(len(pp_dict['name'])):
            file_big.write(pp_dict['name'][i] + ' r=' + pp_dict['r'][i] + ' d=' + pp_dict['d'][i] + ' m=' + pp_dict['m'][i] + '\n' \
                           + pp_dict['x'][i] + ' ' + pp_dict['y'][i] + ' ' + pp_dict['z'][i] + '\n' \
                               + pp_dict['vx'][i] + ' ' + pp_dict['vy'][i] + ' ' + pp_dict['vz'][i] + '\n' \
                                   + pp_dict['Sx'][i] + ' ' + pp_dict['Sy'][i] + ' ' + pp_dict['Sz'][i] + '\n')
        
    # ------------------------------------------------------------------------------------------------
    # Close the big.in file 
    # ------------------------------------------------------------------------------------------------     
    file_big.close()



    # =================================================================================================
    # In this section the small.in files are generated
    # ------------------------------------------------------------------------------------------------
    # Create the small.in file in a specific directory, open it and write in it 
    # ------------------------------------------------------------------------------------------------
    default_file_name = "small.in"
    file_path = os.path.join(default_out_dir, default_file_name)
    file_small = open(file_path, "w+")


    file_small.write(")O+_06 Small-body initial data  (WARNING: Do not delete this line!!) \
                   \n) Lines beginning with `)' are ignored. \
                   \n)---------------------------------------------------------------------\
                   \n  style (Cartesian, Asteroidal, Cometary) = Asteroidal \
                   \n)---------------------------------------------------------------------\n")
        
    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the inner planetesimals
    # ------------------------------------------------------------------------------------------------ 
    if PLANETESIMALS:
        pl_dict = {}

        # Generate the names of the protoplanets
        list_of_names = []
        name = "P{number}"  # E stands for terrestrial planet embryos
        for i in range(nb_pl):
            list_of_names.append(name.format(number = i))    

        # Generate the lists of r, d, m, a, e, i, g (argument of pericenter), n (longitude of ascending node), M (mean anomaly), S (spin)
        list_of_r = [r_pp for i in range(nb_pl)]
        list_of_d = [d_pl for i in range(nb_pl)]
        list_of_m = [M_pl for i in range(nb_pl)]
        list_of_a = np.array(a_annulus(r_in, r_out, nb_pl))
        list_of_e = np.array(e_i_generator(nb_pl)[0])
        list_of_i = np.array(e_i_generator(nb_pl)[1])
        list_of_g = np.array(g_n_generator(nb_pl)[0])
        list_of_n = np.array(g_n_generator(nb_pl)[1])
        list_of_M = np.array(M_generator(nb_pl))
        list_of_S = [S_pp for i in range(nb_pl)]
            
        # In the dictionnary, put the lists as arrays of strings
        pl_dict['name'] = np.array(list_of_names)
        pl_dict['r'] = np.array(list_of_r)
        pl_dict['d'] = np.array(list_of_d)
        pl_dict['m'] = np.array(list_of_m)
        pl_dict['a'] = np.asarray(list_of_a, dtype=str)
        pl_dict['e'] = np.asarray(list_of_e, dtype=str)
        pl_dict['i'] = np.asarray(list_of_i, dtype=str)
        pl_dict['g'] = np.asarray(list_of_g, dtype=str)
        pl_dict['n'] = np.asarray(list_of_n, dtype=str)
        pl_dict['M'] = np.asarray(list_of_M, dtype=str)
        pl_dict['Sx'] = np.array(list_of_S)
        pl_dict['Sy'] = np.array(list_of_S)
        pl_dict['Sz'] = np.array(list_of_S)

        # Write the asteroidal coordinates of the inner planetesimals in the small.in file
        for i in range(len(pl_dict['name'])):
            file_small.write(pl_dict['name'][i] + ' r=' + pl_dict['r'][i] + ' d=' + pl_dict['d'][i] + ' m=' + pl_dict['m'][i] + '\n' \
                           + pl_dict['a'][i] + ' ' + pl_dict['e'][i] + ' ' + pl_dict['i'][i] + '\n' \
                               + pl_dict['g'][i] + ' ' + pl_dict['n'][i] + ' ' + pl_dict['M'][i] + '\n' \
                                   + pl_dict['Sx'][i] + ' ' + pl_dict['Sy'][i] + ' ' + pl_dict['Sz'][i] + '\n')
        

           
    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the outer planetesimals
    # ------------------------------------------------------------------------------------------------ 
    if COMETS: 
        co_dict = {}

        # Generate the names of the protoplanets
        list_of_names = []
        name = "K{number}"  # E stands for terrestrial planet embryos
        for i in range(nb_co):
            list_of_names.append(name.format(number = i))    

        # Generate the lists of r, d, m, a, e, i, g (argument of pericenter), n (longitude of ascending node), M (mean anomaly), S (spin)
        list_of_r = [r_pp for i in range(nb_co)]
        list_of_d = [d_co for i in range(nb_co)]
        list_of_m = [M_co for i in range(nb_co)]
        list_of_a = np.array(a_annulus(rc_in, rc_out, nb_co))
        list_of_e = np.array(e_i_generator(nb_co)[0])
        list_of_i = np.array(e_i_generator(nb_co)[1])
        list_of_g = np.array(g_n_generator(nb_co)[0])
        list_of_n = np.array(g_n_generator(nb_co)[1])
        list_of_M = np.array(M_generator(nb_co))
        list_of_S = [S_pp for i in range(nb_co)]
            
        # In the dictionnary, put the lists as arrays of strings
        co_dict['name'] = np.array(list_of_names)
        co_dict['r'] = np.array(list_of_r)
        co_dict['d'] = np.array(list_of_d)
        co_dict['m'] = np.array(list_of_m)
        co_dict['a'] = np.asarray(list_of_a, dtype=str)
        co_dict['e'] = np.asarray(list_of_e, dtype=str)
        co_dict['i'] = np.asarray(list_of_i, dtype=str)
        co_dict['g'] = np.asarray(list_of_g, dtype=str)
        co_dict['n'] = np.asarray(list_of_n, dtype=str)
        co_dict['M'] = np.asarray(list_of_M, dtype=str)
        co_dict['Sx'] = np.array(list_of_S)
        co_dict['Sy'] = np.array(list_of_S)
        co_dict['Sz'] = np.array(list_of_S)

        # Write the asteroidal coordinates of the inner planetesimals in the small.in file
        for i in range(len(co_dict['name'])):
            file_small.write(co_dict['name'][i] + ' r=' + co_dict['r'][i] + ' d=' + co_dict['d'][i] + ' m=' + co_dict['m'][i] + '\n' \
                           + co_dict['a'][i] + ' ' + co_dict['e'][i] + ' ' + co_dict['i'][i] + '\n' \
                               + co_dict['g'][i] + ' ' + co_dict['n'][i] + ' ' + co_dict['M'][i] + '\n' \
                                   + co_dict['Sx'][i] + ' ' + co_dict['Sy'][i] + ' ' + co_dict['Sz'][i] + '\n')

        
    # ------------------------------------------------------------------------------------------------
    # Close the small.in file 
    # ------------------------------------------------------------------------------------------------     
    file_small.close()    