"""
File: genga_visustatic.py

This routine generates static plots to visualize the outputs of the Genga simulations. 
The eccentricity or semi-major axis can be displayed as a function of time

Author: Sarah Joiret 

Version   Date            Editor          Comment
-------   ----------   --------------   ----------------------------------------------------------
0.1       14 June 2022   Sarah Joiret     First tests.
"""

import numpy as np
import matplotlib.pyplot as plt
import glob
plt.rcParams['animation.ffmpeg_path'] = '/opt/homebrew/bin/ffmpeg'
from astropy.constants import G, M_sun, au 

def cart_2_kep(x, y, z, vx, vy, vz):
    """
    Converts cartesian coordinates to Keplerian coordinates

    Parameters
    ----------
    x : float
        position (x-axis)
    y : float
        position (y-axis)
    z : float
        position (z-axis)
    vx : float
        velocity (x-axis) in AU/day' (must be multiplied by 0.01720209895 to be converted in AU/day)
    vy : float
        velocity (y-axis) in AU/day' (must be multiplied by 0.01720209895 to be converted in AU/day)
    vz : float
        velocity (z-axis) in AU/day' (must be multiplied by 0.01720209895 to be converted in AU/day)

    Returns
    -------
    a, e: float
        Keplerian coordinates of the protoplanets / planetesimals 

    """
    # Convert the units of the velocity from AU/day' to AU/day
    vx = vx * 0.01720209895
    vy = vy * 0.01720209895
    vz = vz * 0.01720209895
    
    # Compute the radius, r, and velocity v
    r = np.sqrt(x**2 + y**2 + z**2)
    v = np.sqrt(vx**2 + vy**2 + vz**2)
    
    # Compute vector r and vector v
    r_vector = np.array([x, y, z])
    v_vector = np.array([vx, vy, vz])
    
    # Compute the specific angular momentum
    h_vector = np.cross(r_vector, v_vector)
    h = np.sqrt(h_vector[0]**2 + h_vector[1]**2 + h_vector[2]**2)
    
    # Compute the Standard gravitational parameter in AU/day
    mu_ms = G.value * M_sun.value # Standard gravitational parameter = GM of the central body
                                  # with G the Newtonian constant of gravitation (in m^3s^-2) 
    mu = mu_ms * (86400**2) / ((au.value)**3)    # There are 86400 s in a Julian day 
    
    # Compute the specific energy E
    E = ((v**2) / 2) - (mu / r)
    
    # Compute semi-major axis a
    a = - mu / (2 * E)
    
    # Compute eccentricity e
    e = np.sqrt(1 - (h**2 / (a * mu)))
    
    return a, e
       
'''
filename = '/Users/sarahjoiret/Desktop/sims/genga/case2_output/big/jovend/3/Outjovend_p000193.dat'
data = np.loadtxt(filename)
time = data[:,0]
mass = data[:,2]
x = data[:,4]
y = data[:,5]
z = data[:,6]
vx = data[:,7]
vy = data[:,8]
vz = data[:,9]

len_time = len(time)
a = np.zeros(len_time)
e = np.zeros(len_time)
q = np.zeros(len_time)
for i in range(len_time):
    a[i] = cart_2_kep(x[i], y[i], z[i], vx[i], vy[i], vz[i])[0]
    e[i] = cart_2_kep(x[i], y[i], z[i], vx[i], vy[i], vz[i])[1]
    q[i] = a[i] *(1 - e[i])
    
'''
    
# Create the figure
fig, ax = plt.subplots()
for filename in glob.glob('/Users/sarahjoiret/Desktop/sims/genga/case2_output/big/clement/10000comets/1/Outclement_p00000*.dat', recursive=True):
    print(filename)
    data = np.loadtxt(filename)
    if data.shape == (21,):
        time = np.asarray([data[0]])
        mass = np.asarray([data[2]])
        x = np.asarray([data[4]])
        y = np.asarray([data[5]])
        z = np.asarray([data[6]])
        vx = np.asarray([data[7]])
        vy = np.asarray([data[8]])
        vz = np.asarray([data[9]])
        
    else:
        time = data[:,0]
        mass = data[:,2]
        x = data[:,4]
        y = data[:,5]
        z = data[:,6]
        vx = data[:,7]
        vy = data[:,8]
        vz = data[:,9]
        
    len_time = len(time)
    #a = np.zeros(len_time)
    #e = np.zeros(len_time)
    a = np.array([])
    e = np.array([])
    print(a)
    for i in range(len_time):
        aa = cart_2_kep(x[i], y[i], z[i], vx[i], vy[i], vz[i])[0]
        ee = cart_2_kep(x[i], y[i], z[i], vx[i], vy[i], vz[i])[1]
        a = np.append(a, aa)
        e = np.append(e, ee)

    ax.plot(time, a*(1-e))
    ax.plot(time, a*(1+e))

    #ax.plot(time, q)
    #ax.plot(time, a)
    #ax.plot(time, e)
    ax.set_xlabel('Time (in years)', fontsize=12)
    ax.set_ylabel('q and Q', fontsize=12)
    ax.vlines(x=5.1e6, ymin=-0.1, ymax=35, color='black')
    ax.set_ylim(-0.1,35)
    #ax.set_xlim(0, 10e6)
