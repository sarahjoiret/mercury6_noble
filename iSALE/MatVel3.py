import pySALEPlot as psp
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from numpy import arange, sqrt, ma

# Need this for the colorbars we will make on the mirrored plot
from mpl_toolkits.axes_grid1 import make_axes_locatable

# If viridis colormap is available, use it here
try:
    plt.set_cmap('viridis')
except:
    plt.set_cmap('YlGnBu_r')

# distances between tracers
def get_distances(s, line):
    x = s.xmark[line]
    y = s.ymark[line]
    return sqrt((x[:-1] - x[1:])**2 + (y[:-1] - y[1:])**2)

# Define the maximum separation allowed when plotting lines
maxsep = 3.

# Make an output directory
dirname = 'MatVel'
psp.mkdir_p(dirname)

# Open the datafile
model = psp.opendatfile('mars/jdata.dat')

# Set the distance units to km
model.setScale('km')

# Set up a pylab figure
#fig, ax = plt.subplots(4, 1, sharex=True, sharey=True, figsize=(6, 14))
#fig.subplots_adjust(hspace=0.0)

# Set up the figure with larger size but adjust subplot area to maintain dimensions
fig = plt.figure(figsize=(8, 14))  # Increase width to add space for the colorbar
grid_spec = fig.add_gridspec(5, 1, left=0.35, right=0.95, hspace=0.0)  # Allocate 70% for subplots

# Create subplots
ax = [fig.add_subplot(grid_spec[i, 0]) for i in range(5)]

plt.xticks(fontsize=16)
plt.xlabel('r [km]', fontsize=24)

# Loop over timesteps with an interval of 10
for j, timestep in enumerate(range(0, 200, 40)):  # Adjust range to include the desired timesteps
    # Set the axis labels
    if j < 4:
        ax[j].xaxis.set_major_locator(ticker.NullLocator())
        ax[j].xaxis.set_major_formatter(ticker.NullFormatter())
    ax[j].set_ylabel('z [km]', fontsize=24)
    ax[j].tick_params(axis='y', which='major', labelsize=16)

    # Set the axis limits
    ax[j].set_xlim([-60, 60])  #ax[j].set_xlim([-100, 100])
    ax[j].set_ylim([-25, 50])  #ax[j].set_ylim([-50, 70])

    # Read the specified timestep from the datafile
    step = model.readStep('VEL', timestep)

    # Plot the first field (material map)
    p1 = ax[j].pcolormesh(model.x, model.y, step.mat,
                          cmap='Oranges', vmin=1, vmax=model.nmat + 1)

    # Plot the second field using negative x values
    p2 = ax[j].pcolormesh(-model.x, model.y, step.data[0],
                          vmin=0, vmax=5050)

    # Material boundaries
    [ax[j].contour(model.xc, model.yc, step.cmc[mat], 1, colors='k', linewidths=0.5) for mat in [0, 1, 2]]
    [ax[j].contour(-model.xc, model.yc, step.cmc[mat], 1, colors='k', linewidths=0.5) for mat in [0, 1, 2]]

    # Add the title inside the subplot, in the upper right corner
    ax[j].text(0.95, 0.95, '{: 2.0f} s'.format(step.time), transform=ax[j].transAxes,
               fontsize=24, ha='right', va='top', bbox=dict(boxstyle="round,pad=0.3", edgecolor='none', facecolor='white'))

# Add a single colorbar for the entire figure, positioned on the left
cbar_ax = fig.add_axes([0.14, 0.15, 0.05, 0.7])  # Adjust position as needed
cbar = fig.colorbar(p2, cax=cbar_ax) #, pad=0.8
cbar.ax.set_ylabel('Velocity (m/s)', fontsize=24) #labelpad=20
cbar.ax.yaxis.set_label_position('left')  # Move label to the left
cbar.ax.yaxis.set_ticks_position('left')
cbar.ax.tick_params(labelsize=18)

# Save the figure
fig.savefig('stack2.png', dpi=300)