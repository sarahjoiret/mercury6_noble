import pySALEPlot as psp
import matplotlib.pyplot as plt
from numpy import arange,sqrt,ma
import os 
import numpy as np 
from pylab import figure,arange,colorbar 

# define function to find ejected tracers
def ejected_tracers(imp_ntracers, x0, y0, x, y, t0, t):
    v_ej = 5030 #m/s #11.2 # km/s
    fast = []
    velocities = []
    dt = t - t0

    for n in range(imp_ntracers):
        dx = x[n]-x0[n]
        dy = y[n]-y0[n]
        vx = dx*1000/dt
        vy = dy*1000/dt
        
        velocity = np.sqrt(vx**2 + vy**2)
        velocities.append(velocity)
        
       # take tracers with positive velocity, ejected at escape velocity
        if vy > 0. and velocity >= v_ej:
            fast.append(n)
    return(np.array(fast)), np.array(velocities)

# Open the datafile
model=psp.opendatfile('mars/jdata.dat')

# Set the distance units to km
model.setScale('km')

# Tracer information is held in model.tru[u], for each tracer cloud, 'u'. 
# You can print some information to the screen using: 
[model.tru[u].truInfo() for u in range(model.tracer_numu)] 

# Initialize variables to track the maximum velocity and corresponding timestep
max_velocity_sum = 0
timestep_of_max_velocity_sum = 0

# make a list for tracers
tracers = []

# loop through the model to get the tracers
for i in range(model.nsteps-1):  # model.nsteps gives the total number of timesteps in the model
    # open two consecutive timesteps
    step0 = model.readStep(['Trx', 'Try'], i)
    stepi = model.readStep(['Trx', 'Try'], i+1)
    # collect the fast tracers
    imp_ntracers = model.tru[0].end  # number of tracers in the impactor
    list_ejected = ejected_tracers(imp_ntracers, step0.Trx, step0.Try, stepi.Trx, stepi.Try, step0.time, stepi.time)[0]
    nb_ejected = len(list_ejected)
    for item in list_ejected:
        if item not in tracers:
            tracers.append(item)
    
    # Calculate velocities of tracers        
    velocities = ejected_tracers(imp_ntracers, step0.Trx, step0.Try, stepi.Trx, stepi.Try, step0.time, stepi.time)[1]
    
    # Calculate the sum of all tracer velocities in this timestep
    total_velocity_sum = np.sum(velocities)
    #print(total_velocity_sum)
    
    # Update global velocity sum and corresponding timestep
    if total_velocity_sum > max_velocity_sum:
        max_velocity_sum = total_velocity_sum
        timestep_of_max_velocity_sum = i
            
#print(ejected_tracers(imp_ntracers, step0.Trx, step0.Try, stepi.Trx, stepi.Try, step0.time, stepi.time))
#print(tracers)
print(len(tracers))
#print(nb_ejected)
print(imp_ntracers)
print('Percentage of ejected material')
print(len(tracers)*100/imp_ntracers)
#print(f"Highest total velocity sum: {max_velocity_sum:.2f} m/s")
#print(f"Occurred at timestep: {timestep_of_max_velocity_sum}")
