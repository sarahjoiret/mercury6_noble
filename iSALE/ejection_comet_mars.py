import pySALEPlot as psp
import matplotlib.pyplot as plt
from numpy import arange,sqrt,ma
import os 
import numpy as np 
from pylab import figure,arange,colorbar 

# define function to find ejected tracers
def ejected_tracers(imp_ntracers, x0, y0, x, y, t0, t):
    v_ej = 5030 #m/s #11.2 # km/s
    fast = []
    dt = t - t0

    for n in range(imp_ntracers):
        dx = x[n]-x0[n]
        dy = y[n]-y0[n]
        vx = dx*1000/dt
        vy = dy*1000/dt
        
        velocity = np.sqrt(vx**2 + vy**2)

       # take tracers with positive velocity, ejected at escape velocity
        if vy > 0. and velocity >= v_ej:
            fast.append(n)
    return(np.array(fast))

# Function to calculate ejected material percentage for a single simulation
def calculate_ejected_percentage(simulation_path):
    # Open the datafile
    model = psp.opendatfile(simulation_path)

    # Set the distance units to km
    model.setScale('km')

    # Initialize a list for tracers
    tracers = []

    # Loop through timesteps
    for i in range(model.nsteps - 1):  # model.nsteps gives the total number of timesteps in the model
        # Open two consecutive timesteps
        step0 = model.readStep(['Trx', 'Try'], i)
        stepi = model.readStep(['Trx', 'Try'], i + 1)
        
        # Collect the fast tracers
        imp_ntracers = model.tru[0].end  # Number of tracers in the impactor
        list_ejected = ejected_tracers(imp_ntracers, step0.Trx, step0.Try, stepi.Trx, stepi.Try, step0.time, stepi.time)
        
        # Add new ejected tracers to the list
        for item in list_ejected:
            if item not in tracers:
                tracers.append(item)

    # Calculate percentage of ejected material
    percentage_ejected = len(tracers) * 100 / imp_ntracers
    return percentage_ejected

# List of simulation paths and corresponding impact velocities
simulation_dirs = [
    ('comet_Mars_10km_10kms/mars/jdata.dat', 10),
    ('comet_Mars_10km_20kms/mars/jdata.dat', 20),
    ('comet_Mars_10km_35kms/mars/jdata.dat', 35),
    ('comet_Mars_10km_50kms/mars/jdata.dat', 50),
    ('comet_Mars_10km_65kms/mars/jdata.dat', 65)
]
impact_velocities = []  # To store impact velocities
ejected_percentages = []  # To store ejected percentages

# Loop over simulations
for sim_dir, velocity in simulation_dirs:
    print(f"Processing simulation: {sim_dir}")
    percentage = calculate_ejected_percentage(sim_dir)
    impact_velocities.append(velocity)
    ejected_percentages.append(percentage)
    print(f"Impact Velocity: {velocity} km/s, Ejected Percentage: {percentage:.2f}%")

# Plot percentage of ejected material vs impact velocity
plt.figure(figsize=(8, 6))
plt.plot(impact_velocities, ejected_percentages, marker='o', linestyle='-', color='b')
plt.xlabel('Impact Velocity (km/s)', fontsize=14)
plt.ylabel('Ejected Material Percentage (%)', fontsize=14)
plt.title('Impact of a 10-km comet on Mars', fontsize=16)
plt.grid(True)
plt.tight_layout()
plt.savefig('ejected_material_vs_velocity2.png', dpi=300)