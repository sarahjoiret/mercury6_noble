"""
File: output_visu.py

This routine generates animated plots to visualize the outputs of the Mercury simulations. 
As a first step the masses M will be displayed as a function of semi-major axes a.
Secondly the eccentricity is displayed as a function of semi-major axis (with the size of the dot
                                                                         proportional to the mass)

Author: Sarah Joiret and Alexandre Mechineau

Version   Date            Editor          Comment
-------   ----------   --------------   ----------------------------------------------------------
0.1       20 Apr 2022   Sarah Joiret     First tests.
"""

import numpy as np
from matplotlib import animation
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
#from matplotlib.animation import PillowWriter
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
import glob
plt.rcParams['animation.ffmpeg_path'] = '/opt/homebrew/bin/ffmpeg'

class body:
    time:np.ndarray
    a:np.ndarray
    e:np.ndarray
    mass:np.ndarray
    
    def __init__(self, filename: str):
        # Load the data from the .aei file
        data = np.loadtxt(filename, skiprows=4)
        
        # If only one line in the .aei file, the data are not a 2-D list and the values
        # must be loaded differently
        if data.shape == (9,):
            self.time = np.asarray([data[0]])
            self.a = np.asarray([data[1]])
            self.e = np.asarray([data[2]])
            self.mass = np.asarray([data[7]])
            
        else:
            self.time = data[:,0]
            self.a = data[:,1]
            self.e = data[:,2]
            self.mass = data[:,7]

        self.len_time = len(self.time)

# Initialize the body list
body_list = []

# Find the directory in which the data are stored and fetch all files from the directory
for filename in glob.glob('/Users/sarahjoiret/Desktop/sims/mercury/case2_output/big/nojov/1/*.aei', recursive=True):
    body_list.append(body(filename))
    
# -----------------------------------------------------------------------------------------------
# FIGURE 1: temporal evolution of the mass as a function of the semi major-axis
# -----------------------------------------------------------------------------------------------

# Create the figure
fig, ax = plt.subplots()
scatter = ax.scatter([body.a[0] for body in body_list],[body.mass[0] for body in body_list], c= 'black', marker='.')
ax.set_xlabel('semi-major axis a/AU', fontsize=12)
ax.set_ylabel('mass M/$M_\u2609$', fontsize=12)

# Define the interval of time for the animation
dt = 1000 # in years

# Animation function
def animate(i):
    
    # Clear the plot for each iteration
    ax.clear()
    
    # iteration of time
    t = i * dt
    
    # Insert images of the terrestrial planets
    mercury = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mercury.png')
    venus = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/venus.png')
    earth = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/earth.png')
    mars = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mars.png')

    im_mercury = OffsetImage(mercury, zoom=0.4)
    im_venus = OffsetImage(venus, zoom=0.4)
    im_earth = OffsetImage(earth, zoom=0.4)
    im_mars = OffsetImage(mars, zoom=0.4)

    ab_me = AnnotationBbox(im_mercury, (0.387, 1.651e-7), frameon=False)
    ab_ve = AnnotationBbox(im_venus, (0.723, 2.447e-6), frameon=False)
    ab_ea = AnnotationBbox(im_earth, (1.0, 3.003e-6), frameon=False)
    ab_ma = AnnotationBbox(im_mars, (1.524, 3.2e-7), frameon=False)
     
    ax.add_artist(ab_me)
    ax.add_artist(ab_ve)
    ax.add_artist(ab_ea)
    ax.add_artist(ab_ma)
    
    # For each body of the body list, take the semi-major axis of the corresponding 
    # iteration time
    a = [  body.a[i] if i < body.len_time else None   for body in body_list ]
    # For each body of the body list, take the mass of the corresponding iteration time
    M =  [  body.mass[i] if i < body.len_time else None   for body in body_list ]
    
    # Plotting the planetesimals 
    ax.set_xlabel('semi-major axis a/AU', fontsize=12)
    ax.set_ylabel('mass M/$M_\u2609$', fontsize=12)
    ax.set_xlim(0.3, 2.2)
    ax.set_ylim(-0.1e-6, 3.2e-6)
    ax.set_title('Time = ' + str(t/1000000) + ' Myrs')
    ax.scatter(a, M, c= 'black', marker='.')
    
    # Plot the terrestrial planet as dots (Mercury, Venus, Earth and Mars)
    #ax.scatter(0.387, 1.651e-7, color='saddlebrown', marker='D', label='Mercury')
    #ax.scatter(0.723, 2.447e-6, color='sandybrown', marker='D', label='Venus')
    #ax.scatter(1.0, 3.003e-6, color='royalblue', marker='D', label='Earth')
    #ax.scatter(1.524, 3.2e-7, color='firebrick', marker='D', label='Mars')
    #ax.legend()

# Call the animation
ani = FuncAnimation(fig, animate, frames=10001, interval=0.001, repeat=False)

# Show the animated plot
plt.show()

# Save the animated plot
#writer = PillowWriter(fps=1000)
#ani.save("terrestrial1.gif", writer=writer)

# -----------------------------------------------------------------------------------------------
# FIGURE 2: temporal evolution of the eccentricity as a function of the semi major-axis
# -----------------------------------------------------------------------------------------------
# Create the figure
fig2, ax2 = plt.subplots()
a_0 = [body.a[0] for body in body_list]
e_0 = [body.e[0] for body in body_list]
ax2.scatter(a_0, e_0, c= 'black', marker='.')
ax2.set_xlabel('semi-major axis a/AU', fontsize=12)
ax2.set_ylabel('eccentricity e', fontsize=12)
ax2.set_title('Time = 0 Myrs')
ax2.set_xlim(0.3, 2.2)
ax2.set_ylim(-0.05, 1.05)

# Animation function
def animate2(j):
    
    # Clear the plot for each iteration
    ax2.clear()
    
    # iteration of time
    t = j * dt
    
    # Insert images of the terrestrial planets
    mercury = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mercury.png')
    venus = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/venus.png')
    earth = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/earth.png')
    mars = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mars.png')

    im_mercury = OffsetImage(mercury, zoom=0.4*0.36) # should be *0.09 to be on scale
    im_venus = OffsetImage(venus, zoom=0.4*1.37)
    im_earth = OffsetImage(earth, zoom=0.4*1.67)
    im_mars = OffsetImage(mars, zoom=0.4*0.72)  # should be * 0.18 to be on scale

    ab_me = AnnotationBbox(im_mercury, (0.387, 0.2056), frameon=False)
    ab_ve = AnnotationBbox(im_venus, (0.723, 0.0068), frameon=False)
    ab_ea = AnnotationBbox(im_earth, (1.0, 0.01671), frameon=False)
    ab_ma = AnnotationBbox(im_mars, (1.524, 0.0935), frameon=False)
     
    ax2.add_artist(ab_me)
    ax2.add_artist(ab_ve)
    ax2.add_artist(ab_ea)
    ax2.add_artist(ab_ma)
    
    # For each body of the body list, take the semi-major axis of the corresponding 
    # iteration time
    a_none = [  body.a[j] if j < body.len_time else None   for body in body_list ]
    # Remove the "None" values
    a = []
    for val_a in a_none:
        if val_a != None :
            a.append(val_a)

    # For each body of the body list, take the eccentricity of the corresponding iteration time
    e_none =  [  body.e[j] if j < body.len_time else None   for body in body_list ]
    # Remove the "None" values
    e = []
    for val_e in e_none:
        if val_e != None :
            e.append(val_e)
    
    # For each body of the body list, take the mass of the corresponding iteration time
    M_none =  [  body.mass[j] * 5e8 if j < body.len_time else None   for body in body_list ]
    # Remove the "None" values
    M = []
    for val_M in M_none:
        if val_M != None :
            M.append(val_M)

    # Plotting the planetesimals 
    ax2.set_xlabel('semi-major axis a/AU', fontsize=12)
    ax2.set_ylabel('eccentricity e', fontsize=12)
    ax2.set_xlim(0.3, 2.2)
    ax2.set_ylim(-0.05, 1.05)
    ax2.set_title('Time = ' + str(t/1000000) + ' Myrs')
    ax2.scatter(a, e, c= 'black', s=M, marker='.')

# Call the animation
ani2 = FuncAnimation(fig2, animate2, frames=10001, interval=0.001, repeat=False)

# Show the animated plot
plt.show()
f = r"c://Users/sarahjoiret/Desktop/sims/mercury/case2_output/big/nojov/1/animation.mp4" 
writervideo = animation.FFMpegWriter(fps=60)
ani2.save(f, writer=writervideo)
